## CentOS 7.1 minimal installation image for HardKernel ODROID XU3/XU4 hardware. 

This image is a PoC to configure CentOS 7 on the ODROID XU3/XU4 (http://www.hardkernel.com/main/products/prdt_info.php) to use it as an RPM build platform.

The rootfs is derived from the CubieTruck CentOS 7 image built by Johnny Hughes and available here (http://people.centos.org/hughesjr/armv7hl/cubietruck/images/)

Unlike the CubieTruck image, this image does not use a 4.x series kernel but instead uses the modified kernel image compiled by HardKernel (3.10.82) which has been taken from the HardKernel Ubuntu image (http://odroid.com/dokuwiki/doku.php?id=en:xu3_release_linux_ubuntu).

**Changes to the CubieTruck rootfs:**
- Set root password to ***cendroid*** from blank to allow SSH from first boot
- Configure network interface to use DHCP on boot (old style interface naming, gig eth is eth0)
- Remove references to swap and /boot partitions from fstab (not used in this image)
- Relabel the root filesystem to 'centos7-root' and mount using LABEL instead of UUID
- Remove 4.x kernel components (modules etc) that were included in CubieTruck base image
- Remove auditd and avahi from starting on boot due to minor errors (will follow up)

The image contains ~250 packages. There is no X-Windows currently installed or configured in the base image.

I have only tested on ODROID XU4 booting from MicroSD, but I am keen to hear any feedback from others if you decide to give it a try.

**TODO:**
- Move to using RootFS Build Factory instead of consuming upstream rootfs image (https://github.com/mndar/rbf)
- Adjust partitioning to reduce whitespace (~2.4G)
- Create an image with X-Windows

**Changelog:**
- 2015-10-05 0.2
  - Removed x86 repo references from yum.repos.d so yum now works 'out of the box' against the C7 ARM repositories
- 2015-10-01 0.1
  - Initial release

**Images:**
- http://pixeldrift.net/arm/cendroid.xu4.0.2.img.gz (273M)
  - md5sum: ea8c17747243f8d15996cb2e31a82082
- http://pixeldrift.net/arm/cendroid.xu4.0.2.img.xz (174M)
  - md5sum: 2cc816ed1782d4c4ca0e09b3d65b0e5a